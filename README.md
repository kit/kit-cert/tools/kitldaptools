# kitldaptools

Specialized query tool for KIT's LDAP server.

## Installation

Get [compiled binaries from Gitlab CI](https://git.scc.kit.edu/KIT-CERT/kitldaptools/-/releases).

## Configuration

See [`example_kit-ldap-query.yaml`](example_kit-ldap-query.yaml) for example config. Place global config file in `/etc/kit-ldap-query.yaml` and user specific config file in `~/.config/kit-ldap-query.yaml`.

Server, port and bind credentials can be set/overwritten at runtime using environment variables or commandline options.

## Usage

See `kit-ldap-query --help` for exact usage information. Most subcommands have multiple aliases. The debian package contains bash completion.

### Get groups for account

Use `kit-ldap-query groups [accountname]` to see all groups this account is a member of.

```bash
$ kit-ldap-query groups tg0457
KIT-Misc-Staff-IDM
SCC-Entitlement-bwCloud-Extended-gesamt
SCC-Entitlement-bwGrid
SCC-Entitlement-bwLSDF-FS
SCC-Entitlement-bwUniCluster
SCC-Entitlement-bwsyncnshare
SCC-Entitlement-bwsyncnshare-idm
SCC-FMC-Users
SCC-Misc-IDM
SCC-Misc-Staff-IDM
[…]
```

### Get group members

Use `kit-ldap-query members [groupname]` to see all accounts that are members of this group.

Special accounts (everything matching `*-adm-*`) will have their metadata fetched from their respective base account.

```bash
$ kit-ldap-query members SCC-demo-fakepeople
tg0457  Chipmann, Chippi (SCC)  tg0457@partner.kit.edu
ugcne           ugcne@student.kit.edu
scc-adm-bb4711	Beate Beispiel	Beate.Beispiel@kit.edu
```

If no group `groupname` can be found `kit-ldap-query` will present a list of all groups that contain `groupname`.

```bash
$ kit-ldap-query members KIT-VPN
No such group »KIT-VPN«. Try one of those:
  KIT-VPN-Alle
  KIT-VPN-Benutzer
```
