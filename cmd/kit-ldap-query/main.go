package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"sort"
	"strings"
	"text/template"
	"time"

	"github.com/mitchellh/go-homedir"
	"gopkg.in/yaml.v2"

	"github.com/go-ldap/ldap/v3"
	"github.com/urfave/cli/v2"
)

var (
	configfileLocations = []string{"/etc/kit-ldap-query.yaml", "~/.config/kit-ldap-query.yaml"}

	// goreleaser variables
	version     = "unknown"
	commit      = "unknown"
	date        = "unknown"
	builtBy     = "unknown"
	compileDate time.Time
	appMetadata = make(map[string]interface{})

	emptyStringReplacer StringReplacer = NOPStringReplacer
)

// Config holds the global configuration
type Config struct {
	Servername           string `yaml:",omitempty"`
	ServerPort           uint   `yaml:",omitempty"`
	BindDN               string `yaml:",omitempty"`
	BindPassword         string `yaml:",omitempty"`
	EmptyStringIndicator string `yaml:",omitempty"`
	FieldSeparator       string `yaml:",omitempty"`
}

// StringReplacer replaces a string with a different one
type StringReplacer func(input string) string

// NOPStringReplacer is the identity function for type StringReplacer
func NOPStringReplacer(input string) string { return input }

var (
	QueryTemplateGroups         = template.Must(template.New("groups").Parse(`(&(objectClass=posixGroup)(memberUid={{- .Query -}}))`))
	QueryTemplateGroupsWildcard = template.Must(template.New("groupwild").Parse(`(&(objectClass=posixGroup)(cn=*{{- .Query -}}*))`))
	QueryTemplateGroupMembers   = template.Must(template.New("groupmembers").Parse(`(&(objectClass=posixGroup)(cn={{- .Query -}}))`))
	QueryTemplateMemberInfo     = template.Must(template.New("memberinfo").Parse(`(&(objectClass=posixAccount)(objectClass=inetOrgPerson)(cn={{- .Query -}}))`))

	RegexAdmAccount = regexp.MustCompile(`(?P<oe>[[:alnum:]]+)-adm-(?P<samaccountname>[[:alnum:]]+)`)

	config Config
)

func QueryGroupsFilter(query string) string {
	var b bytes.Buffer
	_ = QueryTemplateGroups.Execute(&b, struct{ Query string }{ldap.EscapeFilter(query)})
	return b.String()
}

func QueryGroupWildcardFilter(query string) string {
	var b bytes.Buffer
	_ = QueryTemplateGroupsWildcard.Execute(&b, struct{ Query string }{ldap.EscapeFilter(query)})
	return b.String()
}

func QueryGroupMembersFilter(query string) string {
	var b bytes.Buffer
	_ = QueryTemplateGroupMembers.Execute(&b, struct{ Query string }{ldap.EscapeFilter(query)})
	return b.String()
}

func QueryMemberInfoFilter(query string) string {
	var b bytes.Buffer
	_ = QueryTemplateMemberInfo.Execute(&b, struct{ Query string }{ldap.EscapeFilter(query)})
	return b.String()
}

func init() {
	var (
		err         error
		configfiles = configfileLocations
	)

	// bare minimum default config
	config = Config{
		Servername: "kit-ldap-01.scc.kit.edu",
		ServerPort: 636,
	}

	// parse compile time
	compileDate, err = time.Parse("2006-01-02T15:04:05Z", date)
	if err != nil {
		compileDate, _ = time.Parse("2006-01-02T15:04:05Z", "1999-12-24T08:15:00Z")
	}
	appMetadata["version"] = version
	appMetadata["commit"] = commit
	appMetadata["build date"] = date
	appMetadata["builtBy"] = builtBy

	log.SetFlags(0)

	// expand ~ if present
	for idx, c := range configfiles {
		expandedFilename, err := homedir.Expand(c)
		if err != nil {
			log.Fatalf("Unable to expand config filename: %s", err)

		}
		configfiles[idx] = expandedFilename
	}

	// read config files in order
	for _, c := range configfiles {
		contents, err := ioutil.ReadFile(c)
		if err != nil {
			continue
		}
		err = yaml.Unmarshal(contents, &config)
		if err != nil {
			log.Fatalf("Unable to decode config file %s: %s", c, err)
		}
	}
}

func ProcessConfig(c *Config) {
	if len(c.Servername) == 0 {
		log.Fatalf("Please specify a server name.")
	}
	if len(c.BindDN) == 0 {
		log.Fatalf("Please specify a bind DN.")
	}
	if len(c.BindPassword) == 0 {
		log.Fatalf("Please specify a password.")
	}

	if c.EmptyStringIndicator != "" {
		emptyStringReplacer = func(input string) string {
			if input != "" {
				return input
			}
			return c.EmptyStringIndicator
		}
	}
}

// LDAPConnect connects and binds to an LDAP server
func LDAPConnect(c *Config) *ldap.Conn {
	var (
		err        error
		connectURL string
		conn       *ldap.Conn
	)
	if c.ServerPort == 0 {
		connectURL = fmt.Sprintf("ldaps://%s", c.Servername)

	} else {
		connectURL = fmt.Sprintf("ldaps://%s:%d", c.Servername, c.ServerPort)
	}
	conn, err = ldap.DialURL(connectURL)
	if err != nil {
		log.Fatalf("Error connecting to KIT LDAP: %s", err)
	}

	err = conn.Bind(c.BindDN, c.BindPassword)
	if err != nil {
		log.Fatalf("Error binding to KIT LDAP: %s", err)
	}

	return conn
}

func LDAPGetGroups(conn *ldap.Conn, querystring string) []string {
	var groups []string

	// create LDAP search
	query := ldap.NewSearchRequest(
		"ou=unix,ou=IDM,dc=kit,dc=edu",
		ldap.ScopeWholeSubtree,
		ldap.DerefAlways,
		0, 0, false,
		QueryGroupsFilter(querystring),
		[]string{"cn"},
		nil,
	)

	// search LDAP
	results, err := conn.Search(query)
	if err != nil {
		log.Fatalf("Error searching LDAP: %s", err)
	}

	// process results
	if len(results.Entries) == 0 {
		log.Fatalf("No results found for »%s«", querystring)
	}
	for _, r := range results.Entries {
		groups = append(groups, r.GetAttributeValue("cn"))
	}
	sort.Strings(groups)
	return groups
}

func LDAPGetMembers(conn *ldap.Conn, querystring string) []string {
	var (
		members       []string
		memberStrings []string
		query         *ldap.SearchRequest
		results       *ldap.SearchResult
		err           error
	)

	// create LDAP search
	query = ldap.NewSearchRequest(
		"ou=unix,ou=IDM,dc=kit,dc=edu",
		ldap.ScopeWholeSubtree,
		ldap.DerefAlways,
		0, 0, false,
		QueryGroupMembersFilter(querystring),
		[]string{"memberUid"},
		nil,
	)

	// search LDAP
	results, err = conn.Search(query)
	if err != nil {
		log.Fatalf("Error searching LDAP")
	}

	// process results
	if len(results.Entries) == 0 {
		// create LDAP search (look for alternatives)
		query = ldap.NewSearchRequest(
			"ou=unix,ou=IDM,dc=kit,dc=edu",
			ldap.ScopeWholeSubtree,
			ldap.DerefAlways,
			0, 0, false,
			QueryGroupWildcardFilter(querystring),
			[]string{"cn"},
			nil,
		)

		// search LDAP
		results, err = conn.Search(query)
		if err != nil {
			log.Fatalf("Error searching LDAP")
		}

		if len(results.Entries) > 0 {
			var suggestions []string
			for _, s := range results.Entries {
				suggestions = append(suggestions, "  "+s.GetAttributeValue("cn"))
			}
			sort.Strings(suggestions)
			_, _ = fmt.Fprintf(os.Stderr, "No such group »%s«. Try one of those:\n%s\n", querystring, strings.Join(suggestions, "\n"))
			_ = cli.Exit("No matching group found", 128)
		} else {
			log.Printf("Unable to find group »%s« and unable to find matching suggestions.", querystring)
			_ = cli.Exit("No matching group found", 128)
		}

	}
	for _, r := range results.Entries {
		members = append(members, r.GetAttributeValues("memberUid")...)
	}
	sort.Strings(members)

	// get info for every member
	for _, uid := range members {
		uidstring := uid

		// special case: *-adm-*
		admMatch := RegexAdmAccount.FindStringSubmatch(uid)
		if len(admMatch) == 3 {
			uidstring = admMatch[2]
		}
		// create LDAP search
		query = ldap.NewSearchRequest(
			"ou=unix,ou=IDM,dc=kit,dc=edu",
			ldap.ScopeWholeSubtree,
			ldap.DerefAlways,
			0, 0, false,
			QueryMemberInfoFilter(uidstring),
			[]string{"displayName", "mail"},
			nil,
		)

		// search LDAP
		results, err = conn.Search(query)
		if err != nil {
			log.Fatalf("Error looking up display name for %s", uid)
		}

		var displayName = ""
		var mailAddr = ""
		if len(results.Entries) > 0 {
			displayName = results.Entries[0].GetAttributeValue("displayName")
			mailAddr = results.Entries[0].GetAttributeValue("mail")
		}

		memberStrings = append(memberStrings, strings.Join(
			[]string{
				emptyStringReplacer(uid),
				emptyStringReplacer(displayName),
				emptyStringReplacer(mailAddr),
			}, config.FieldSeparator),
		)
	}
	sort.Strings(memberStrings)
	return memberStrings
}

func main() {
	var (
		conn *ldap.Conn
	)

	app := &cli.App{
		Name:                 "kit-ldap-query",
		Usage:                "Query KIT-LDAP",
		EnableBashCompletion: true,
		Version:              version,
		Authors: []*cli.Author{
			{
				Name:  "Heiko Reese",
				Email: "heiko.reese@kit.edu",
			},
		},
		Compiled:  compileDate,
		Copyright: "MIT License",
		Metadata:  appMetadata,
		Commands: []*cli.Command{
			{
				Name:      "groups",
				Aliases:   []string{"showgroups", "g"},
				Flags:     nil,
				Usage:     "List all groups an account is a member of",
				UsageText: "groups [accountname]",
				Action: func(c *cli.Context) error {
					ProcessConfig(&config)
					if c.NArg() == 0 {
						log.Fatalf("Please specify a username to search for")
					}
					if c.NArg() > 1 {
						log.Printf("Found %d search strings instead of one. Ignoring everything except »%s«", c.NArg(), c.Args().First())
					}
					conn = LDAPConnect(&config)
					groups := LDAPGetGroups(conn, c.Args().First())
					fmt.Println(strings.Join(groups, "\n"))
					return nil
				},
			},
			{
				Name:        "members",
				Aliases:     []string{"showmembers", "m"},
				Flags:       nil,
				Usage:       "List all members of given group",
				UsageText:   "members [groupname]",
				Description: "If a group is not found in KIT-LDAP a list of possible groups is shown based on a simple subtext search",
				Action: func(c *cli.Context) error {
					ProcessConfig(&config)
					if c.NArg() == 0 {
						log.Fatalf("Please specify a group name to search for")
					}
					if c.NArg() > 1 {
						log.Printf("Found %d search strings instead of one. Ignoring everything except »%s«", c.NArg(), c.Args().First())
					}
					conn = LDAPConnect(&config)
					members := LDAPGetMembers(conn, c.Args().First())
					fmt.Println(strings.Join(members, "\n"))
					return nil
				},
			},
			{
				Name:  "dumpconfigyaml",
				Usage: "Write an empty YAML config to stdout and exit",
				Action: func(context *cli.Context) error {
					y, err := yaml.Marshal(Config{
						Servername:           "kit-ldap-01.scc.kit.edu",
						ServerPort:           636,
						BindDN:               "uid=EDIT-THIS-ENTRY,ou=ProxyUser,ou=IDM,dc=kit,dc=edu",
						BindPassword:         "insert secret here",
						EmptyStringIndicator: "∅",
					})
					if err != nil {
						return err
					}
					fmt.Print(string(y))
					return nil
				},
			},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "server",
				Aliases:     []string{"s"},
				Usage:       "KIT-LDAP server name",
				EnvVars:     []string{"KIT_LDAP_SERVER"},
				Destination: &config.Servername,
				Value:       config.Servername,
			},
			&cli.UintFlag{
				Name:        "port",
				Usage:       "KIT-LDAP server port",
				EnvVars:     []string{"KIT_LDAP_SERVER_PORT"},
				Value:       config.ServerPort,
				Destination: &config.ServerPort,
				HasBeenSet:  true,
			},
			&cli.StringFlag{
				Name:        "binddn",
				Aliases:     []string{"u"},
				Usage:       "KIT-LDAP login username",
				EnvVars:     []string{"KIT_LDAP_BINDDN"},
				Destination: &config.BindDN,
				Value:       config.BindDN,
			},
			&cli.StringFlag{
				Name:        "bindpw",
				Aliases:     []string{"p"},
				Usage:       "KIT-LDAP login password",
				EnvVars:     []string{"KIT_LDAP_BINDPASS"},
				Destination: &config.BindPassword,
				Value:       config.BindPassword,
				DefaultText: "*redacted*",
			},
			&cli.StringFlag{
				Name:        "fieldsep",
				Aliases:     []string{"fs", "fieldseparator"},
				Usage:       "Separator to use for tabular output",
				EnvVars:     []string{"KIT_LDAP_QUERY_FIELDSEP"},
				Value:       "\t",
				DefaultText: "TAB",
				Destination: &config.FieldSeparator,
			},
			&cli.StringFlag{
				Name:        "emptystring",
				Aliases:     []string{"es"},
				Usage:       "Replace certain empty output strings with this string",
				EnvVars:     []string{"KIT_LDAP_QUERY_EMPTYSTRING"},
				Destination: &config.EmptyStringIndicator,
				Value:       config.EmptyStringIndicator,
			},
		},
	}
	app.EnableBashCompletion = true

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}
