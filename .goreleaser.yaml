project_name: kit-ldap-query
dist: build

before:
  hooks:
    - go mod tidy

builds:
  - main: ./cmd/kit-ldap-query
    id: "kit-ldap-query"
    binary: kit-ldap-query
    targets:
      - "darwin_amd64_v1"
      - "darwin_arm64"
      - "linux_amd64_v1"
      - "windows_amd64_v1"
    ldflags: &ldflags
      - "-s -w -extldflags '-static'"
      - "-X main.version={{.Version}}"
      - "-X main.commit={{.Commit}}"
      - "-X main.date={{.Date}}"
      - "-X main.builtBy=goreleaser"
    env: &env
      - CGO_ENABLED=0

upx:
  -
    enabled: true
    compress: best
    brute: true

checksum:
  name_template: 'checksums.txt'

gitlab_urls:
  api: https://gitlab.kit.edu/api/v4
  download: https://gitlab.kit.edu
  use_package_registry: false
  use_job_token: false

snapshot:
  name_template: "{{ incpatch .Version }}-next"

changelog:
  sort: asc
  filters:
    exclude:
      - '^docs:'
      - '^test:'

release:
  name_template: "{{.ProjectName}} {{.Version}}"
  prerelease: auto

nfpms:
  -
    id: kit-ldap-query
    package_name: kit-ldap-query
    maintainer: Heiko Reese <heiko.reese@kit.edu>
    description: |-
      Simple commandline client for KIT-LDAP.
    license: MIT
    formats:
      - deb
      - rpm
    section: default
    priority: extra
    contents:
      - src: README.md
        dst: /usr/share/doc/kit-ldap-query/README.md
      - src: example_kit-ldap-query.yaml
        dst: /etc/kit-ldap-query.yaml
        type: "config|noreplace"
      - src: extra/bash_autocomplete
        dst: /usr/share/bash-completion/completions/kit-ldap-query
      - src: extra/zsh_autocomplete
        dst: /usr/share/zsh/vendor-completions/_kit-ldap-query
    overrides:
      deb:
        dependencies:
          - ca-certificates
          - bash-completion
    # Custom configuration applied only to the Deb packager.
    deb:
      # Lintian overrides
      lintian_overrides:
        - statically-linked-binary
        - changelog-file-missing-in-native-package
    rpm:
      compression: lzma
